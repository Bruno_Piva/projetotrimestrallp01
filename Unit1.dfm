object Formulario: TFormulario
  Left = 0
  Top = 0
  Caption = 'Formulario'
  ClientHeight = 229
  ClientWidth = 447
  Color = clHighlight
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 13
  object Ed_poder: TEdit
    Left = 88
    Top = 8
    Width = 121
    Height = 21
    TabOrder = 0
    TextHint = 'PODER'
  end
  object Ed_nomeheroi: TEdit
    Left = 88
    Top = 40
    Width = 121
    Height = 21
    TabOrder = 1
    TextHint = 'NOME DO HEROI'
  end
  object Ed_nomeverdadeiro: TEdit
    Left = 88
    Top = 72
    Width = 161
    Height = 21
    TabOrder = 2
    TextHint = 'NOME VERDADEIRO DO HEROI'
  end
  object Bt_inserir: TButton
    Left = 296
    Top = 8
    Width = 75
    Height = 25
    Caption = 'inserir'
    TabOrder = 3
    OnClick = Bt_inserirClick
  end
  object Bt_carregar: TButton
    Left = 296
    Top = 137
    Width = 75
    Height = 25
    Caption = 'carregar'
    TabOrder = 4
    OnClick = Bt_carregarClick
  end
  object Bt_deletar: TButton
    Left = 296
    Top = 39
    Width = 75
    Height = 25
    Caption = 'deletar'
    TabOrder = 5
    OnClick = Bt_deletarClick
  end
  object Bt_salvar: TButton
    Left = 296
    Top = 106
    Width = 75
    Height = 25
    Caption = 'salvar'
    TabOrder = 6
    OnClick = Bt_salvarClick
  end
  object Bt_atualizar: TButton
    Left = 296
    Top = 70
    Width = 75
    Height = 25
    Caption = 'atualizar'
    TabOrder = 7
    OnClick = Bt_atualizarClick
  end
  object ListBox_herois: TListBox
    Left = 88
    Top = 112
    Width = 121
    Height = 97
    ItemHeight = 13
    TabOrder = 8
    OnClick = ListBox_heroisClick
  end
end
