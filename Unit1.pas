unit Unit1;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.StdCtrls;

type
  TFormulario = class(TForm)
    Ed_nomeverdadeiro: TEdit;
    Bt_inserir: TButton;
    Bt_deletar: TButton;
    Bt_atualizar: TButton;
    ListBox_herois: TListBox;
    Ed_nomeheroi: TEdit;
    Ed_poder: TEdit;
    Bt_salvar: TButton;
    Bt_carregar: TButton;
    procedure Bt_inserirClick(Sender: TObject);
    procedure Bt_deletarClick(Sender: TObject);
    procedure Bt_atualizarClick(Sender: TObject);
    procedure Bt_salvarClick(Sender: TObject);
    procedure Bt_carregarClick(Sender: TObject);
    procedure ListBox_heroisClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;
 THeroi = class(TObject)
    nome:string;
    nomeVerdadeiro:string;
    poder:string;
  end;
var
  Formulario: TFormulario;

implementation

{$R *.dfm}

procedure TFormulario.Bt_atualizarClick(Sender: TObject);
var heroi1:THeroi;
var nome, nomeverdadeiro, poder: string;
begin
  nome:=Ed_nomeheroi.Text;
  nomeverdadeiro:=Ed_nomeverdadeiro.Text;
  poder:= Ed_poder.Text;

  if (nome.Length<>0) and (nomeverdadeiro.Length<>0) and (poder.Length<>0)  then
  begin
    heroi1 :=ListBox_herois.Items.Objects[ListBox_herois.ItemIndex] as THeroi;
    heroi1.nome:= nome;
    heroi1.nomeverdadeiro:= nomeverdadeiro;
    heroi1.poder:= poder;
    ListBox_herois.Items[ListBox_herois.ItemIndex]:= Ed_nomeheroi.Text ;
  end
  else
  begin
    ShowMessage('Campo Vazio!!! Por favor preencha.');
  end;

end;

procedure TFormulario.Bt_deletarClick(Sender: TObject);
begin
      ListBox_herois.DeleteSelected;
end;

procedure TFormulario.Bt_carregarClick(Sender: TObject);
var linha, nome, nomeverdadeiro, poder: string;
var i:integer;
var arq:TextFile;
var heroi1:THeroi;
begin
  AssignFile(arq, 'Herois.txt');
  Reset(arq);
  heroi1:=THeroi.Create;
  while (not Eof(arq)) do
  begin
      Readln(arq, linha);


      i:= pos('|', linha);
      nome:= copy(linha, 1, i-1);
      delete(linha, 1, i);

      i:= pos('|', linha);
      nomeverdadeiro:= copy(linha, 1, i-1);
      delete(linha, 1, i);

      i:= pos('|', linha);
      poder:= copy(linha, 1, i-1);

      heroi1.nome:=nome;
      heroi1.nomeverdadeiro:=nomeverdadeiro;
      heroi1.poder:=poder;
      ListBox_herois.Items.AddObject(heroi1.nome, heroi1);
  end;
    CloseFile(arq);

end;

procedure TFormulario.Bt_inserirClick(Sender: TObject);
var heroi:THeroi;
var nome, nomeverdadeiro, poder: string;
begin
  nome:=Ed_nomeheroi.Text;
  nomeverdadeiro:=Ed_nomeverdadeiro.Text;
  poder:= Ed_poder.Text;

  if (nome.Length<>0) and (nomeverdadeiro.Length<>0) and (poder.Length<>0)  then
  begin
    heroi := THeroi.Create;
    heroi.nome := nome;
    heroi.nomeverdadeiro := nomeverdadeiro;
    heroi.poder:= poder;
    ListBox_herois.Items.AddObject(heroi.nome, heroi);
  end
  else
  begin
    ShowMessage('Campo Vazio!!! Por favor preencha.');
  end;

end;

procedure TFormulario.Bt_salvarClick(Sender: TObject);
var arq : TextFile;
var heroi1:THeroi;
var i: integer;
begin
      if ListBox_herois.Items.Count<>0 then
      begin
      AssignFile(arq, 'Herois.txt');
      Rewrite(arq);
      for i:=0 to Pred(ListBox_herois.Items.Count) do
      begin
        heroi1:= ListBox_herois.Items.Objects[i] as THeroi;
        writeln(arq, heroi1.nome+'|'+heroi1.nomeverdadeiro+'|'+heroi1.poder+'|');
      end;

      CloseFile(arq);
    end
    else
    begin
      ShowMessage('Nenhum item para salvar!');
    end;
end;
procedure TFormulario.ListBox_heroisClick(Sender: TObject);
begin
Bt_deletar.Enabled:=true;
Bt_atualizar.Enabled:=true;
end;

end.
